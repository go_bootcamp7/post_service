package service

import (
	"context"

	"github.com/jmoiron/sqlx"
	p "gitlab.com/learn-microservice/post_service/genproto/post"
	"gitlab.com/learn-microservice/post_service/genproto/user"
	"gitlab.com/learn-microservice/post_service/pkg/logger"
	grpcclient "gitlab.com/learn-microservice/post_service/service/grpc_client"
	"gitlab.com/learn-microservice/post_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type PostService struct {
	storage storage.IStorage
	Logger  logger.Logger
	Client  grpcclient.Clients
}

func NewPostService(db *sqlx.DB, log logger.Logger, client grpcclient.Clients) *PostService {
	return &PostService{
		storage: storage.NewStoragePg(db),
		Logger:  log,
		Client:  client,
	}
}

func (s *PostService) CreatePost(ctx context.Context, req *p.PostRequest) (*p.PostResponse, error) {
	res, err := s.storage.Post().CreatePost(req)
	if err != nil {
		s.Logger.Error("error insert post", logger.Any("Error insert post", err))
		return &p.PostResponse{}, status.Error(codes.Internal, "something went wrong, please check user info")
	}
	return res, nil
}

func (s *PostService) GetPostById(ctx context.Context, req *p.PostId) (*p.PostResponse, error) {
	res, err := s.storage.Post().GetPostById(req.Id)
	if err != nil {
		s.Logger.Error("error get post", logger.Any("Error get post", err))
		return &p.PostResponse{}, status.Error(codes.Internal, "something went wrong, please check user info")
	}
	user, err := s.Client.User().GetUserByPostId(ctx, &user.PostId{PostId: req.Id})
	if err != nil {
		return &p.PostResponse{}, err
	}
	res.FirstName = user.FirstName
	return res, nil
}

func (s *PostService) GetPostByUserId(ctx context.Context, req *p.UserId) (*p.Posts, error) {
	res, err := s.storage.Post().GetPostByUserId(req.Id)
	if err != nil {
		s.Logger.Error("error get post by user_id", logger.Any("Error get post by user_id", err))
		return &p.Posts{}, status.Error(codes.Internal, "something went wrong, please check post info")
	}
	return res, nil
}
