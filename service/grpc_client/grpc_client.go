package grpcclient

import (
	"fmt"

	"gitlab.com/learn-microservice/post_service/config"
	cu "gitlab.com/learn-microservice/post_service/genproto/user"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type Clients interface {
	User() cu.UserServiceClient
}

type ServiceManager struct {
	Config      config.Config
	userService cu.UserServiceClient
}

func New(cfg config.Config) (*ServiceManager, error) {
	connUser, err := grpc.Dial(
		fmt.Sprintf("%s:%s", cfg.UserServiceHost, cfg.UserServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, fmt.Errorf("user service dial host:%s port: %s", cfg.UserServiceHost, cfg.UserServicePort)
	}

	return &ServiceManager{
		Config:      cfg,
		userService: cu.NewUserServiceClient(connUser),
	}, nil

}

func (s *ServiceManager) User() cu.UserServiceClient {
	return s.userService
}
