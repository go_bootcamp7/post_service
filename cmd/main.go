package main

import (
	"fmt"
	"net"

	"gitlab.com/learn-microservice/post_service/config"
	p "gitlab.com/learn-microservice/post_service/genproto/post"
	"gitlab.com/learn-microservice/post_service/pkg/db"
	"gitlab.com/learn-microservice/post_service/pkg/logger"
	"gitlab.com/learn-microservice/post_service/service"
	grpcclient "gitlab.com/learn-microservice/post_service/service/grpc_client"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	cfg := config.Load()
	log := logger.New(cfg.LogLevel, "golang")
	defer logger.Cleanup(log)

	connDb, err := db.ConnectToDB(cfg)
	if err != nil {
		fmt.Println("Error connect postgres", err.Error())
	}

	grpcClient, err := grpcclient.New(cfg)
	if err != nil {
		fmt.Println("Error while grpc client", err.Error())
	}

	postService := service.NewPostService(connDb, log, grpcClient)

	lis, err := net.Listen("tcp", cfg.PostServicePort)

	if err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}

	s := grpc.NewServer()
	reflection.Register(s)
	p.RegisterPostServiceServer(s, postService)
	log.Info("main: server running",
		logger.String("port", cfg.PostServicePort))
	if err := s.Serve(lis); err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}

}
