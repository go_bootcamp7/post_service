package postgres

import (
	"database/sql"

	p "gitlab.com/learn-microservice/post_service/genproto/post"
)

func (r *PostRepo) CreatePost(post *p.PostRequest) (*p.PostResponse, error) {
	res := p.PostResponse{}
	err := r.db.QueryRow(`
	INSERT INTO 
		post(user_id, title,description) 
	VALUES
		($1, $2, $3) 
	RETURNING 
		id,user_id, title, description, created_at, updated_at`, post.UserId, post.Title, post.Description).
		Scan(&res.Id, &res.UserId, &res.Title, &res.Description, &res.CreatedAt, &res.UpdatedAt)
	if err != nil {
		return &p.PostResponse{}, err
	}

	return &res, nil
}

func (r *PostRepo) GetPostById(id int64) (*p.PostResponse, error) {
	res := p.PostResponse{}
	err := r.db.QueryRow(`
	SELECT 
		id, title, description, created_at, updated_at
	FROM 
		post
	WHERE
		id=$1`, id).
		Scan(&res.Id, &res.Title, &res.Description, &res.CreatedAt, &res.UpdatedAt)
	if err != nil {
		return &p.PostResponse{}, err
	}
	return &res, nil
}

func (r *PostRepo) GetPostByUserId(id int64) (*p.Posts, error) {
	var res p.Posts

	rows, err := r.db.Query(`
	SELECT 
		id, user_id, title, description, created_at, updated_at
	FROM 
		post
	WHERE 
		user_id=$1`, id)
	if err == sql.ErrNoRows {
		return &p.Posts{}, nil
	}
	for rows.Next() {
		temp := p.PostResponse{}
		err = rows.Scan(&temp.Id, &temp.UserId,
			&temp.Title, &temp.Description,
			&temp.CreatedAt, &temp.UpdatedAt)
		if err != nil {
			return &p.Posts{}, err
		}
		res.Posts = append(res.Posts, &temp)
	}
	return &res, nil
}
