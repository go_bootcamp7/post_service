package repo

import (
	p "gitlab.com/learn-microservice/post_service/genproto/post"
)

type PostStorageI interface {
	CreatePost(*p.PostRequest) (*p.PostResponse, error)
	GetPostById(id int64) (*p.PostResponse, error)
	GetPostByUserId(id int64) (*p.Posts, error)
}
