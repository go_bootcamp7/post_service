package storage

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/learn-microservice/post_service/storage/postgres"
	"gitlab.com/learn-microservice/post_service/storage/repo"
)

type IStorage interface {
	Post() repo.PostStorageI
}

type storagePg struct {
	db       *sqlx.DB
	postRepo repo.PostStorageI
}

func NewStoragePg(db *sqlx.DB) *storagePg {
	return &storagePg{
		db:       db,
		postRepo: postgres.NewPostRepo(db),
	}
}

func (s storagePg) Post() repo.PostStorageI {
	return s.postRepo
}
