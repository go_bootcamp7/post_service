run:
	go run cmd/main.go

proto-gen:
	./script/gen-proto.sh

migrate_up:
	migrate -path migrations/ -database postgres://developer:2002@localhost:5432/post_db up

update_submodule:
	git submodule update --remote --merge

migrate_down:
	migrate -path migrations/ -database postgres://developer:2002@localhost:5432/post_db down


migrate_force:
	migrate -path migrations/ -database postgres://developer:2002@localhost:5432/post_db forse 1


migrate_file:
	migrate create -ext sql -dir migrations -seq create_post_table

